import React, { useState, useEffect } from 'react';

export function View() {
 const [photoData, setPhotoData] = useState (null);

const a = async function fetchPhoto(){
    const res = await fetch(
           `https://api.nasa.gov/planetary/apod?api_key=hgAgKDyz0whohu9RX6aSW9IdID2XRTlZLB2eBQhM`
       );
       const data = await res.json(); 
       setPhotoData(data);
       console.log(data);
}
 
 useEffect(() => { a();
 }, []);

 if (!photoData) return <div />;

 return (
    <div>
        <img src={photoData.url} alt={photoData.title} />
    </div>
 );


}
export default View;