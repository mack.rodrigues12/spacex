import React from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './components/Header';
import Error from './components/Error';
import Missions from './components/Missions';
import Rockets from './components/Rockets';
import MyProfile from './components/MyProfile';
import View from './components/ViewFromSpace/View';
import './App.css';

function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/" component={Rockets} />
        <Route path="/missions" component={Missions} />
        <Route path="/myprofile" component={MyProfile} />
        <Route path="*" component={Error} />
        <Route path="/view" component={View} />
      </Switch>
    </Router>
  );
}

export default App;
